package com.sinosig.sparksql

import org.apache.log4j.{Logger,Level}
import org.apache.spark.{SparkConf, SparkContext}

/**
  *更详细的请查看spark官方文档，@see <a href="http://spark.apache.org/docs/1.6.3/sql-programming-guide.html#hive-tables">Spark SQL, DataFrames and Datasets Guide</a>
  *
  * Created by yore
  */
object MyHiveSql extends App {
  Logger.getLogger("org.apache.hadoop").setLevel(Level.ERROR)
  Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)
  Logger.getLogger("org.eclipse.jetty.server").setLevel(Level.OFF)

  val conf = new SparkConf().setAppName("spark-hive").setMaster("local[1]")

  val sc = new SparkContext(conf)
  val sqlContext = new org.apache.spark.sql.hive.HiveContext(sc)
  sqlContext.sql("use adl")
  val df = sqlContext.sql("select count(*) from b010_business_contract")
  df.show()

  sc.stop()

}
