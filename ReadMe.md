Spark with Scala DSL Project
===================================

### Spark SQL
[Spark SQL, DataFrames and Datasets Guide](http://spark.apache.org/docs/1.6.3/sql-programming-guide.html#hive-tables)



### 异常解决
##### The root scratch dir: /tmp/hive on HDFS should be writable. 
```
java.lang.RuntimeException: java.lang.RuntimeException: The root scratch dir: /tmp/hive on HDFS should be writable. Current permissions are: rwx------;
```
原因：在win环境新运行spark,local模式下将dataframe的数据写入hive表中，
spark会在本地默认建立一个default的数据库，然后将你的dataframe保存到对应的表中。
而hive默认的数据仓库文件系统路径是：/tmp/hive，这个目录可以在你项目所在的盘符中找到，这个目录是程序运行时生成的。
而这个目录目前的权限是rwx------，还不具备可写权限。

解决：`winutils.exe chmod 777 D:\tmp\hive`

[参考stackoverflow上给出的解决方法](https://stackoverflow.com/questions/34196302/the-root-scratch-dir-tmp-hive-on-hdfs-should-be-writable-current-permissions)


